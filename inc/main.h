/**
  ******************************************************************************
  * @file    Templates/main.h 
  * @author  MCD Application Team
  * @version V1.0.3
  * @date    22-April-2016 
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

//#pragma GCC poison malloc free
/* Includes ------------------------------------------------------------------*/

#include "stm32f7xx_hal.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
#include "stm32f7xx_hal.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_camera_custom.h"
#include "stm32746g_discovery_lcd_custom.h"
#include "face.h"
/* Exported constants --------------------------------------------------------*/
/* LCD Frame Buffer address */


#define RESIZE_BUFFER                     0xC0390000 //1.1875Mbits
#define CAMERA_FRAME_BUFFER               0xC0260000 //1.1875Mbits
#define LCD_FRAME_BUFFER                  0xC0130000 //1.1875Mbits
#define SMALL_FRAME_BUFFER                ((uint32_t) img_face)

#include "stb_image_resize.h"
#define STBIR_MALLOC(size,context) (uint32_t *)RESIZE_BUFFER
#define STBIR_FREE(ptr,context)   
#define STB_IMAGE_RESIZE_IMPLEMENTATION

// #define STBI_MALLOC(sz)           (uint32_t *)RESIZE_BUFFER
// #define STBI_REALLOC(p,newsz)     (uint32_t *)RESIZE_BUFFER
// #define STBI_FREE(p)              


#define camera_resolution      RESOLUTION_R320x240
#define camera_xsize           320
#define camera_ysize           240
#define lcd_xsize           480
#define lcd_ysize           272
#define small_xsize         18
#define small_ysize         18
#define xoffset         0
#define yoffset         0


/* SPI DEFINITIONS */
#define SPI_CPLD_INSTANCE SPI5
#define SPI_CS_CPLD_PORT GPIOF
#define SPI_CS_CPLD_PIN GPIO_PIN_6

#define SPI_MOSI_CPLD_PORT GPIOF
#define SPI_MOSI_CPLD_PIN GPIO_PIN_9
#define SPI_MOSI_CPLD_AF       GPIO_AF5_SPI5

#define SPI_SCLK_CPLD_PORT GPIOF
#define SPI_SCLK_CPLD_PIN GPIO_PIN_7
#define SPI_SCLK_CPLD_AF      GPIO_AF5_SPI5

#define SPI_CPLD_INSTANCE                             SPI5
#define SPI_CPLD_CLK_ENABLE()               __HAL_RCC_SPI5_CLK_ENABLE()
#define SPI_CPLD_SCK_GPIO_CLK_ENABLE()       __HAL_RCC_GPIOF_CLK_ENABLE()
#define SPI_CPLD_MOSI_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOF_CLK_ENABLE()
#define SPI_CPLD_CS_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOF_CLK_ENABLE()

#define SPI_CPLD_FORCE_RESET()               __HAL_RCC_SPI5_FORCE_RESET()
#define SPI_CPLD_RELEASE_RESET()             __HAL_RCC_SPI5_RELEASE_RESET()


/* Definition for SPIx Pins */


#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
