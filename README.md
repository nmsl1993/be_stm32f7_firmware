#Bionic Eye STM32F7 Controller Firmware
The code for running Bionic Eye's STM32F7 MCU and Altera MAX II EPS420 CPLD
##Introduction
Something something modulate video MSK criterion (TODO)
###Useful Resources:

* [STM32F7 - Reference Manual](http://www.st.com/resource/en/reference_manual/dm00124865.pdf)
* [STM32F7 - Programming Manual](http://www.st.com/content/ccc/resource/technical/document/programming_manual/group0/78/47/33/dd/30/37/4c/66/DM00237416/files/DM00237416.pdf/jcr:content/translations/en.DM00237416.pdf)
* [STM32F7-Discovery - User Manual](http://www.st.com/content/ccc/resource/technical/document/user_manual/f0/14/c1/b9/95/6d/40/4d/DM00190424.pdf/files/DM00190424.pdf/jcr:content/translations/en.DM00190424.pdf)
* [STM32F746 - Datasheet](http://www.st.com/content/ccc/resource/technical/document/datasheet/96/ed/61/9b/e0/6c/45/0b/DM00166116.pdf/files/DM00166116.pdf/jcr:content/translations/en.DM00166116.pdf)
* [STM32CubeF4 - Firmware Library](http://www.st.com/en/embedded-software/stm32cubef7.html) (Need to create an account on STM's site - Includes lots of useful example code)
* [MAXII Device Handbook](https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/hb/max2/max2_mii5v1.pdf) (EPM240T CPLD is of the Altera MAX II family)
* [EPM240 DevBoard - Schematic](https://www.openimpulse.com/blog/wp-content/uploads/wpsc/downloadables/EPM240-Minimal-Development-Board-Schematic.pdf)

##Installation
Dependencies:
* arm-none-eabi-*
* stlink
* make
* cmake (for STLINK)
* libusb-1.0.0-dev (for STLINK)
* libgtk-3-dev (for STLINK)

Download GNU ARM Embedded Toolchain 4.9:
https://launchpad.net/gcc-arm-embedded/4.9/4.9-2015-q3-update

Download ST-Link source code:
https://github.com/texane/stlink/archive/master.zip

###Ubuntu Apt command

```
$ sudo apt-get install binutils-arm-none-eabi gcc-arm-none-eabi gdb-arm-none-eabi libnewlib-arm-none-eabi build-essential cmake libusb-1.0.0-dev 
$ cd /tmp
$ wget https://github.com/texane/stlink/archive/1.2.0.zip
$ cd stlink-1.2.0
$ cmake .
$ make
$ sudo make install
$ sudo cp 49-stlinkv2-1.rules /etc/udev/rules.d
$ sudo udevadm control --reload-rules
$ sudo udevadm trigger
```

###Optional Sublime Text 3 
Install sublime-text-3 (optional)
```
$ sudo add-apt-repository ppa:webupd8team/sublime-text-3
$ sudo apt-get-update
$ sudo apt-get install sublime-text-installer
```

Install Package control within Sublime Text (optional)

1. Open Sublime Text
2. Press Ctrl-` (Control key and ~ key at the same time)
3. Paste Package Control Install script in textbox and press enter

```
import urllib.request,os,hashlib; h = 'df21e130d211cfc94d9b0905775a7c0f' + '1e3d39e33b79698005270310898eea76'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by) 
```
4. Install Makefile Improved Sublime Plugin by pressing Ctrl-Shift-P within Sublime Text and typing in "Makefile Improved" 

###Camera Wiring
|Pin      |Connect     |
|---------|------------|
|PA9      | DCMI_D0    |
|PA10     | DCMI_D1    |
|PE0      | DCMI_D2    |
|PE1      | DCMI_D3    |
|PC11     | DCMI_D4    |
|PB6      | DCMI_D5    |
|PE5      | DCMI_D6    |
|PA8      | Camera XCLK|
|PG9      | DCMI_VSYNC |
|PA4      | DCMI_HSYNC |
|PB10     | DCMI_SIOC  |
|PB11     | DCMI_SIOD  |
