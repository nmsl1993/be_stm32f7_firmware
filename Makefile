# Makefile for STM32F7-Discovery-Blinky

PROJECT = be_spi

################
# Sources

STM32CUBEDIR=/opt/STM32Cube_FW_F7_V1.5.0
ARM_TOOLS_BIN_DIR=/opt/gcc-arm-none-eabi/bin
ST_FLASH=st-flash
SOURCES_S = startup/startup_stm32f746xx.s

SOURCES_C = src/main.c
SOURCES_C += src/stm32f7xx_it.c src/system_stm32f7xx.c
SOURCES_C += src/stm32746g_discovery_camera_custom.c
SOURCES_C += src/stm32746g_discovery_lcd_custom.c

SOURCES_C += $(STM32CUBEDIR)/Drivers/BSP/STM32746G-Discovery/stm32746g_discovery.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/BSP/STM32746G-Discovery/stm32746g_discovery_sdram.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/BSP/Components/ov9655/ov9655.c

SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_gpio.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_i2c.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_spi.c

SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_tim.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_tim_ex.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_dma.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_dma2d.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_ltdc.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_sdram.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_dcmi.c

SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_ll_fmc.c

SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_dma_ex.c



SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_cortex.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_rcc.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_rcc_ex.c

SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_pwr.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_pwr_ex.c
SOURCES_C += $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal.c




SOURCES = $(SOURCES_S) $(SOURCES_C) $(SOURCES_CPP)
OBJS = $(SOURCES_S:.s=.o) $(SOURCES_C:.c=.o) $(SOURCES_CPP:.cpp=.o)

################
# Includes and Defines

INCLUDES = -I src -I inc
INCLUDES += -I $(STM32CUBEDIR)/Drivers/BSP/Components/ov9655

INCLUDES += -I $(STM32CUBEDIR)/Drivers/CMSIS/Include
INCLUDES += -I $(STM32CUBEDIR)/Drivers/CMSIS/Device/ST/STM32F7xx/Include
INCLUDES += -I $(STM32CUBEDIR)/Drivers/STM32F7xx_HAL_Driver/Inc
INCLUDES += -I $(STM32CUBEDIR)/Drivers/BSP/STM32746G-Discovery

DEFINES = -DSTM32 -DSTM32F7 -DSTM32F746xx -DSTM32F746NGHx -DSTM32F746G_DISCO

################
# Compiler/Assembler/Linker/etc

PREFIX = $(ARM_TOOLS_BIN_DIR)/arm-none-eabi

CC = $(PREFIX)-gcc
AS = $(PREFIX)-as
AR = $(PREFIX)-ar
LD = $(PREFIX)-gcc
NM = $(PREFIX)-nm
OBJCOPY = $(PREFIX)-objcopy
OBJDUMP = $(PREFIX)-objdump
READELF = $(PREFIX)-readelf
SIZE = $(PREFIX)-size
GDB = arm-none-eabi-gdb
RM = rm -f

################
# Compiler options

MCUFLAGS = -mcpu=cortex-m7 -mlittle-endian
MCUFLAGS += -mfloat-abi=softfp -mfpu=fpv5-sp-d16
MCUFLAGS += -mthumb

DEBUG_FLAGS = -O0 -g -gdwarf-2
RELEASE_FLAGS = -O3 -mtune=cortex-m7
#DEBUGFLAGS = -O2

GDB_FLAGS = -nx -readnow -tui -fullname

OPENOCD_GDB_EVAL_COMMANDS = --eval-command="target extended-remote localhost:3333; mon reset halt; load; mon reset init"
ST_GDB_EVAL_COMMANDS = --eval-command="target extended-remote localhost:4242; mon reset halt; mon reset init"

CFLAGS = -std=c99
CFLAGS += -Wall -Wextra --pedantic

SEMIHOSTING_FLAGS = --specs=rdimon.specs -lc -lrdimon
DEBUG_FLAGS += $(SEMIHOSTING_FLAGS)

CFLAGS_EXTRA = -nostartfiles -fdata-sections -ffunction-sections
CFLAGS_EXTRA += -Wl,--gc-sections -Wl,-Map=$(PROJECT).map

#CFLAGS += $(DEFINES) $(MCUFLAGS) $(DEBUG_FLAGS) $(CFLAGS_EXTRA) $(INCLUDES)
CFLAGS += $(DEFINES) $(MCUFLAGS) $(RELEASE_FLAGS) $(CFLAGS_EXTRA) $(INCLUDES)


LDFLAGS = -static $(MCUFLAGS)
LDFLAGS += -Wl,--start-group -lgcc -lm -lc -lg -Wl,--end-group

LDFLAGS += -Wl,--gc-sections
LDFLAGS += -T STM32F746NGHx_FLASH.ld -L. -Lldscripts
LDFLAGS += -Xlinker -Map -Xlinker $(PROJECT).map

LDFLAGS += $(SEMIHOSTING_FLAGS)

################
# Build rules

all: $(PROJECT).hex


flash: $(PROJECT).bin
	$(ST_FLASH) write $(PROJECT).bin 0x8000000
$(PROJECT).hex: $(PROJECT).elf
	$(OBJCOPY) -O ihex $(PROJECT).elf $(PROJECT).hex

$(PROJECT).bin: $(PROJECT).elf
	$(OBJCOPY) -O binary $(PROJECT).elf $(PROJECT).bin

$(PROJECT).elf: $(OBJS)
	$(LD) $(OBJS) $(LDFLAGS) -o $(PROJECT).elf
	$(SIZE) -A $(PROJECT).elf

gdb: $(PROJECT).elf
	$(GDB) $(GDB_FLAGS) $(OPENOCD_GDB_EVAL_COMMANDS) $(PROJECT).elf

st-gdb: $(PROJECT).elf
	$(GDB) $(GDB_FLAGS) $(ST_GDB_EVAL_COMMANDS) $(PROJECT).elf

clean:
	$(RM) $(OBJS) $(PROJECT).elf $(PROJECT).hex $(PROJECT).map $(PROJECT).bin

# EOF
