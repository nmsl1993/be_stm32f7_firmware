from tkinter import *
from PIL import Image
import numpy as np
name = 'stopsign_320_240color'
ext = '.bmp'
image = Image.open(name+ext) 
#image.show('xv')
img_data = np.asarray(image, dtype=np.uint32)
print(image)
print(img_data.shape)
height = img_data.shape[0]
width = img_data.shape[1]
depth = img_data.shape[2]

#1st arg is Height, 2nd arg Width
carr_txt = '#include <stdint.h>\n\n'
carr_txt += '#define ' + name.upper() + '_HEIGHT %i\n' % height
carr_txt += '#define ' + name.upper() + '_WIDTH %i\n\n' % width

carr_txt += "__ALIGN_END const uint8_t img_%s[%i][%i]  = {\n" % (name,height,width)
for row in range(0,height-1):
	carr_txt += ','.join(map(hex,img_data[row,:])) + ',\n'

carr_txt += ','.join(map(hex,img_data[row+1,:])) + '};'
#print(carr_txt)

with open(name+'.h','w') as f:
	f.write(carr_txt)
print(name+'.h written')
