
#define __ALIGN_END 
#include "stopsign_320_240gs.h"

#include <stdio.h>
#pragma GCC poison malloc free //Ensure that we don't use GCC malloc
static uint8_t mem_space[19200];
#define STBIR_MALLOC(size,context) mem_space
#define STBIR_FREE(ptr,context)   
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image_resize.h"
int main()
{

printf("Begin resize test.\n");
uint8_t shrunk_out[18][18];
uint8_t * LCD_FRAME_BUFFER = (uint8_t *) &img_stopsign_320_240gs[0][0];
uint8_t * out = &shrunk_out[0][0];

int res = stbir_resize_uint8(      LCD_FRAME_BUFFER , 320 , 240 , 0,
                               out, 18, 18, 0, 1);
                               



FILE *file = fopen("stopbw.data", "wb");
fwrite(&img_stopsign_320_240gs,  320*240*sizeof(uint8_t), 1, file);
fclose(file);
FILE *file2 = fopen("stopbw_small.data", "wb");
fwrite(&shrunk_out,  18*18*sizeof(uint8_t), 1, file2);
fclose(file2);
}
