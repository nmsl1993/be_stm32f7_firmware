
`timescale 100ns/100ps



module fsk_test;

	reg clk, reset,data,val;
	wire fsk,rdy;
	
	always #5 clk = !clk;

	always @(posedge rdy) begin
		data = $urandom_range(1,0);
	end

	fsk_mod fsm(
		.fsk(fsk),
		.clk(clk),
		.reset(reset),
		.data(data),
		.rdy(rdy),
		.val(val)

	);
	
	initial begin
		clk = 1'b0;
		reset = 1'b1;
		val = 1'b1;
		data = 1'b1;
		$dumpfile("fsk.vcd");
		$dumpvars;
	#10;
	reset = 1'b0;
	#100000;
	val = 1'b0;
	#40000;
	val= 1'b1;
	#100000;

	$finish;
	end

	endmodule
