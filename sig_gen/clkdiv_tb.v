`timescale 1ns/10ps
module clkdiv_tb;
  reg clk,reset;
  wire clk_out1, clk_out2;
 
     clk_divn #(.WIDTH(5), .N(24)) t1(clk,reset,clk_out1);
     clk_divn #(.WIDTH(5), .N(23)) t2(clk,reset,clk_out2);

	  initial
          clk= 1'b0;
     always
        #5  clk=~clk; 
        initial
            begin
               #5 reset=1'b1;
               #10 reset=1'b0;
               #5000 $finish;
            end
 
        //initial
               //$monitor("clk=%b,reset=%b,clk_out=%b",clk,reset,clk_out);
 
        initial
            begin
              $dumpfile("clkdiv_tb.vcd");
              $dumpvars;
             end
endmodule
