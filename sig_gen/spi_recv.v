module spi_recv(
	input reset,
	input spi_cs,
	input spi_clk,
	input spi_dat,
	output spout
	);
	//Corresponds to CPOL=0 CPHA=0 (SPI_MODE0)
	localparam BUFFER_DEPTH = 256;
	reg [BUFFER_DEPTH-1:0] spi_buf;
	always @(posedge spi_clk) begin
		if (reset) begin
			spi_buf <= {{BUFFER_DEPTH}{1'b0}};
		end
		else if (spi_cs == 1'b0) begin //Check for CS low rather than falling edge #hack
			spi_buf[BUFFER_DEPTH-2:0] <= spi_buf[BUFFER_DEPTH-1:1];
			spi_buf[BUFFER_DEPTH-1] <= spi_dat;	
		end
	end
	assign spout = spi_buf[0];
endmodule