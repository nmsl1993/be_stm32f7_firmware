module sig_gen(
	input osc_clk,
	input mcu_clk,
	input spi_dat,
	input spi_clk,
	input spi_cs,
	output mcu_clk_out,
	output led,
	output fsk_out,
	output reset
	);
	reg ledr;
	reg [24:0] count;
	reg [9:0] reset_count; //Unknown initial value, but only ~1/1024 probability of equaling RESET_COUNTER_VALUE so we will likely get an edge on reset line.
	reg data;
	wire clk,rdy;
	assign clk = osc_clk;
	assign reset = (reset_count != RESET_COUNTER_COMPARE);
	parameter RESET_COUNTER_COMPARE = 10'b1010001110; //Some random value
	always @(posedge clk) begin
		count <= count+1;
		if (reset) begin
			reset_count <= reset_count+10'd1;
		end else if (count == 25'd0) begin //Will only blink if we've come out of reset
			ledr <= ~ledr;
		end
		
	end
		assign led = ledr;
		assign mcu_clk_out = mcu_clk;
		
	always @ (posedge rdy) begin
		data = ~data;
	end
	fsk_mod fskm(
			.fsk(fsk_out),
			.val(1'b0),
			.rdy(rdy),
			.data(data), 
			.clk(clk),
			
		   .reset(reset)
	);
	
	spi_recv spr(
		.reset(reset),
		.spi_cs(spi_cs),
		.spi_clk(spi_clk),
		.spi_dat(spi_dat)
		);
endmodule