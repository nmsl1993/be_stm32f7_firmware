module fsk_mod (
	input val,
	input data, 
	input clk,
	input reset,
	output reg fsk,
	output rdy
	);

	parameter DIV_LOW = 5'd12;
	parameter DIV_HIGH = 5'd11;
	parameter NUMBER_OF_PULSES = 5'd24; //24 pulses is 12 cycles

	localparam DLOW = (DIV_LOW/2)-1;
	localparam DHIGH = (DIV_HIGH/2)-1;

	localparam S_CHOOSE = 2'b00;
	localparam S_LOW    = 2'b01;
	localparam S_HIGH   = 2'b10;
	parameter INPUT_BUFFER_LENGTH = 128;

	//reg [1:0] cur_state;
	reg [1:0] cur_state;
	reg [5:0] clkdiv_counter;
	reg [4:0] pulse_counter;

	always @ (posedge clk) begin
		if (reset || ~val) begin
			fsk <= 1'b0;
			clkdiv_counter <= 5'd0;
			pulse_counter <= 5'd0;
			cur_state <= S_CHOOSE; 
		end
		else if (cur_state == S_CHOOSE) begin
			cur_state <= 2'd1+data;
			clkdiv_counter <= 5'd0;
			pulse_counter <= 5'd0;
			fsk <= 1'b0;

		end
		else if (cur_state == S_LOW) begin
			if (pulse_counter<NUMBER_OF_PULSES) begin
				if (clkdiv_counter<DLOW) begin
					clkdiv_counter <= clkdiv_counter + 5'd1;
					pulse_counter <= pulse_counter;
				end
				else begin
					clkdiv_counter <= 5'd0;
					pulse_counter <= pulse_counter + 5'd1;
					fsk <= ~fsk;
				end
				cur_state <= S_LOW;
			end

			else begin
				clkdiv_counter <= 5'd0;
				pulse_counter <= 5'd0;
				cur_state <= S_CHOOSE;
			end
		end
		else if (cur_state == S_HIGH) begin
			if (pulse_counter<NUMBER_OF_PULSES) begin
				if (clkdiv_counter<DHIGH) begin
					clkdiv_counter <= clkdiv_counter + 5'd1;
					pulse_counter <= pulse_counter;
				end
				else begin
					clkdiv_counter <= 5'd0;
					pulse_counter <= pulse_counter + 5'd1;
					fsk <= ~fsk;
				end
				cur_state <= S_HIGH;
			end			
			else begin
				clkdiv_counter <= 5'd0;
				pulse_counter <= 5'd0;
				cur_state <= S_CHOOSE;
			end
		end
	end



endmodule
/*

module fsk_mod (
	input val,
	input data, 
	input clk,
	input reset,
	output fsk,
	output rdy
	);

	parameter DIV_LOW = 5'd27;
	parameter DIV_HIGH = 5'd16;
	parameter NUMBER_OF_PULSES = 4'd12; //24 pulses is 12 cycles

	localparam CLK_COUNT_COMPARE_LOW = DIV_LOW*NUMBER_OF_PULSES;
	localparam CLK_COUNT_COMPARE_HIGH = DIV_HIGH*NUMBER_OF_PULSES;
	localparam S_RESET  = 3'b000;
	localparam S_READY  = 3'b001;
	localparam S_CHOOSE = 3'b010;
	localparam S_LOW    = 3'b011;
	localparam S_HIGH   = 3'b100;
	parameter INPUT_BUFFER_LENGTH = 128;

	reg [2:0] cur_state;
	reg [2:0] next_state;	
	reg [8:0] clk_counter;
	reg ready;

	reg fdiv_high_reset;
	reg fdiv_low_reset;

	wire clk_div_high_out;
	wire clk_div_low_out;
	reg [1:0] fsk_mux_sel;
	assign rdy = ready;

	assign fsk = (fsk_mux_sel == 2'b00) ? 1'b0 : (fsk_mux_sel == 2'b01 ? clk_div_low_out : clk_div_high_out ) ;
	//////COMBINATIONAL LOGIC/////////
	always @ (*) begin
		case(cur_state)
			S_RESET : begin
				fdiv_low_reset = 1'b1;
				fdiv_high_reset = 1'b1;
				ready = 0;
				next_state = S_READY;
				fsk_mux_sel = 2'b00;
			end
			S_READY : begin
				fdiv_low_reset = 1'b1;
				fdiv_high_reset = 1'b1;
				ready = 1;
				next_state = S_CHOOSE;
				fsk_mux_sel = 2'b00;
			end
			S_CHOOSE : begin
				if(val) begin
					ready = 0;
					if(data) begin
						next_state = S_HIGH;
						fdiv_low_reset = 1'b1;
						fdiv_high_reset = 1'b0;
						fsk_mux_sel = 2'b10;
					end else begin
						next_state = S_LOW;
						fdiv_low_reset = 1'b0;
						fdiv_high_reset = 1'b1;
						fsk_mux_sel = 2'b01;
					end
				end else begin
					ready = 1;
					next_state = S_CHOOSE;
					fdiv_low_reset = 1'b1;
					fdiv_high_reset = 1'b1;
					fsk_mux_sel = 2'b00;
				end

			end
			S_LOW : begin
				if(clk_counter == CLK_COUNT_COMPARE_LOW) begin
					ready = 1;
					next_state = S_CHOOSE;
					fsk_mux_sel = 2'b00;
					fdiv_low_reset = 1'b1;
					fdiv_high_reset = 1'b1;				
				end else begin
					ready=0;
					next_state = S_LOW;
					fdiv_low_reset = 1'b0;
					fdiv_high_reset = 1'b1;
					fsk_mux_sel = 2'b01;					
				end
			end
			S_HIGH : begin

				if(clk_counter == CLK_COUNT_COMPARE_HIGH) begin
					ready = 1;
					next_state = S_CHOOSE;
					fsk_mux_sel = 2'b00;
					fdiv_low_reset = 1'b1;
					fdiv_high_reset = 1'b1;	
				end else begin
					next_state = S_HIGH;
					ready=0;
					fdiv_low_reset = 1'b1;
					fdiv_high_reset = 1'b0;
					fsk_mux_sel = 2'b10;
				end
			end
			default : begin
				next_state = S_RESET;
			end
		endcase
	end
	//////////////////////////////////
	/////SEQUENTIAL LOGIC/////////////
	always @ (posedge clk) begin
		if(reset) begin
			cur_state <= S_RESET;
			clk_counter = 9'd0;
		end else begin
			cur_state <= next_state;
			if(next_state == S_HIGH || next_state == S_LOW) begin
				clk_counter = clk_counter+1;
			end else begin
				clk_counter = 9'd0;
			end
		end
	end


	//////////////////////////////////

	clk_divn #(.WIDTH(5),.N(DIV_HIGH)) clk_div_high(
		.clk(clk),
		.reset(fdiv_high_reset),
		.clk_out(clk_div_high_out)
	);
	clk_divn #(.WIDTH(5),.N(DIV_LOW)) clk_div_low(
		.clk(clk),
		.reset(fdiv_low_reset),
		.clk_out(clk_div_low_out)
	);
endmodule
*/